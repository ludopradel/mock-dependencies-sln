namespace mock_dependency_inversion
{
    public interface IRepository
    {
        bool Save(Order order);
    }
}