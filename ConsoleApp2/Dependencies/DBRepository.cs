using System;

namespace mock_dependency_inversion
{
    public class DBRepository : IRepository
    {
        public bool Save(Order order)
        {
            throw new InvalidOperationException("Don't call me directly, use a double instead !");
        }
    }
}
