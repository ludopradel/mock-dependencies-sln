namespace mock_dependency_inversion
{
    public interface ISender
    {
        void SendConfirmationMessage(Order order);
    }
}
