using System;

namespace mock_dependency_inversion
{
    public class MailSender : ISender
    {
        public void SendConfirmationMessage(Order order)
        {
            throw new InvalidOperationException("Don't call me directly, use a double instead !");
        }
    }
}
