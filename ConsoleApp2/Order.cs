namespace mock_dependency_inversion
{
    public class Order
    {

        private string _product;

        public Order(string product)
        {
		_product = product;
        }

        public bool IsValid()
        {
		    return _product == "";
        }
        
    }
}
